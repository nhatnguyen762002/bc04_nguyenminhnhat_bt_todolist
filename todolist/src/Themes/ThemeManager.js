import { DarkTheme } from "./DarkTheme";
import { LightTheme } from "./LightTheme";
import { PrimaryTheme } from "./PrimaryTheme";

export const ThemeArr = [
  { id: 1, name: "Primary theme", theme: PrimaryTheme },
  { id: 2, name: "Light theme", theme: LightTheme },
  { id: 3, name: "Dark theme", theme: DarkTheme },
];
