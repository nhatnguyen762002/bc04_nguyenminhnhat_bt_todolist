export const DarkTheme = {
  bgColor: "#343a40",
  color: "white",
  borderButton: "1px solid white",
  borderRadiusButton: "none",
  hoverTextColor: "#343a40",
  hoverBgColor: "white",
  borderColor: "#343a40",
};
