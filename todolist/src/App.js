import logo from "./logo.svg";
import "./App.css";
import TodoList from "./TodoList";

function App() {
  return (
    <div className="my-4">
      <TodoList />
    </div>
  );
}

export default App;
