import React, { Component, Fragment } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "./Components/Container";
import { Dropdown } from "./Components/Dropdown";
import { Button } from "./Components/Button";
import { Label, Input, TextField } from "./Components/TextField";
import { Table, Thead, Tbody, Tr, Td, Th } from "./Components/Table";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "./Components/Heading";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from "./redux/actions/TodoListActions";
import { ThemeArr } from "./Themes/ThemeManager";

class TodoList extends Component {
  state = {
    taskName: "",
    disabledUpdate: true,
    disabledAdd: false,
  };

  renderTaskTodo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.name}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabledUpdate: false,
                      disabledAdd: true,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
              >
                <i className="fa fa-edit" />
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check" />
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.name}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return ThemeArr.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // Bản <16.4
  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.taskEdit.name,
  //   });
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.theme}>
        <Container className="w-50">
          <Fragment>
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;

                this.props.dispatch(changeThemeAction(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>

            <Heading3 className="mt-3">To do list</Heading3>

            <TextField
              value={this.state.taskName}
              onChange={(e) => {
                this.setState({
                  taskName: e.target.value,
                });
              }}
              label="Task name"
              className="w-50"
            />

            {this.state.disabledAdd ? (
              <Button
                disabled
                onClick={() => {
                  let { taskName } = this.state;

                  let newTask = {
                    id: Date.now(),
                    name: taskName,
                    done: false,
                  };

                  this.props.dispatch(addTaskAction(newTask));
                }}
                className="ml-2"
              >
                <i className="fa fa-plus"></i> Add task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  let { taskName } = this.state;

                  let newTask = {
                    id: Date.now(),
                    name: taskName,
                    done: false,
                  };

                  this.props.dispatch(addTaskAction(newTask));
                }}
                className="ml-2"
              >
                <i className="fa fa-plus"></i> Add task
              </Button>
            )}

            {this.state.disabledUpdate ? (
              <Button
                disabled
                onClick={() => {
                  this.props.dispatch(updateTaskAction(this.state.taskName));
                }}
                className="ml-2"
              >
                <i className="fa fa-upload"></i> Update task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  let { taskName } = this.state;

                  this.setState(
                    {
                      disabledUpdate: true,
                      disabledAdd: false,
                      taskName: "",
                    },
                    () => {
                      this.props.dispatch(updateTaskAction(taskName));
                    }
                  );
                }}
                className="ml-2"
              >
                <i className="fa fa-upload"></i> Update task
              </Button>
            )}
          </Fragment>

          <hr />

          <Fragment>
            <Heading3>Task to do</Heading3>

            <Table>
              <Thead>{this.renderTaskTodo()}</Thead>
            </Table>
          </Fragment>

          <Fragment>
            <Heading3>Task completed</Heading3>

            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Fragment>
        </Container>
      </ThemeProvider>
    );
  }

  // Bản >16.4
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tại thì mình mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.name,
      });
    }
  }
}

const mapStateToProps = (state) => ({
  theme: state.todoListReducer.theme,
  taskList: state.todoListReducer.taskList,
  taskEdit: state.todoListReducer.taskEdit,
});

export default connect(mapStateToProps)(TodoList);
