import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../types/TodoListTypes";

export const addTaskAction = (payload) => ({
  type: ADD_TASK,
  payload,
});

export const changeThemeAction = (payload) => ({
  type: CHANGE_THEME,
  payload,
});

export const doneTaskAction = (payload) => ({
  type: DONE_TASK,
  payload,
});

export const deleteTaskAction = (payload) => ({
  type: DELETE_TASK,
  payload,
});

export const editTaskAction = (payload) => ({
  type: EDIT_TASK,
  payload,
});

export const updateTaskAction = (payload) => ({
  type: UPDATE_TASK,
  payload,
});
