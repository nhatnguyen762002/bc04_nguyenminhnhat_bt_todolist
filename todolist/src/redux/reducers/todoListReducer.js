import { PrimaryTheme } from "../../Themes/PrimaryTheme";
import { ThemeArr } from "../../Themes/ThemeManager";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../types/TodoListTypes";

const initialState = {
  theme: PrimaryTheme,
  taskList: [
    { id: 1, name: "Task 1", done: true },
    { id: 2, name: "Task 2", done: false },
    { id: 3, name: "Task 3", done: false },
    { id: 4, name: "Task 4", done: true },
  ],
  taskEdit: { id: null, name: "", done: false },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK: {
      if (payload.name.trim() === "") {
        alert("vui lòng nhập task name!");
        return state;
      }

      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex((task) => {
        return task.name === payload.name;
      });

      if (index !== -1) {
        alert("Task đã tồn tại!");
        return state;
      }

      taskListUpdate.push(payload);

      state.taskList = taskListUpdate;

      return { ...state };
    }

    case CHANGE_THEME: {
      let themeSelectObj = ThemeArr.find((theme) => theme.id == payload);

      if (themeSelectObj) {
        state.theme = themeSelectObj.theme;
      }

      return { ...state };
    }

    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex((task) => task.id === payload);

      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }

    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex((task) => task.id === payload);

      if (index !== -1) {
        taskListUpdate = taskListUpdate.filter((task) => task.id !== payload);
      }

      return { ...state, taskList: taskListUpdate };
    }

    case EDIT_TASK: {
      return { ...state, taskEdit: payload };
    }

    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, name: payload };

      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(
        (task) => task.id === state.taskEdit.id
      );

      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }

      state.taskList = taskListUpdate;
      state.taskEdit = { id: null, name: "", done: false };

      return { ...state };
    }

    default:
      return state;
  }
};
