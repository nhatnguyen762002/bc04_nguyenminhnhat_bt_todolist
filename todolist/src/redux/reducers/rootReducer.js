import todoListReducer from "./todoListReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
  todoListReducer,
});
